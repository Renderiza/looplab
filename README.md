LoopLab
=======

SketchUp plugin that selects edges that are part of a loop.

Author:: Renderiza
Name:: LoopLab
Version:: 1.0.0
SU Version:: 2013
Date:: 11/8/2013
