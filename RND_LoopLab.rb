module RND_Extensions
	#
	module RND_LoopLab
		require 'sketchup.rb'
		require 'extensions.rb'
		#
		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new 'LoopLab', 'RND_LoopLab/RND_LoopLab_menus.rb'
			#
			# Attach some nice info.
			ext.creator     = 'Renderiza'
			ext.version     = '1.0.0'
			ext.copyright   = '2013, Renderiza'
			ext.description = 'Select edges that are part of a loop.'
			#
			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		#
	end
	#
end
#
file_loaded( __FILE__ )
