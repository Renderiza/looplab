module RND_Extensions
	#
	module RND_LoopLab
		require 'sketchup.rb'
		require 'RND_LoopLab/RND_LoopLab_loader.rb'
	end
	#
	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end
	#
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------
	if !file_loaded?(__FILE__) then
		@@rnd_tools_menu.add_item('LoopLab'){
		Sketchup.active_model.select_tool RND_Extensions::RND_LoopLab::LoopLab.new
		}
		# Add toolbar
		rnd_looplab_tb = UI::Toolbar.new "LoopLab"
		rnd_looplab_cmd = UI::Command.new("LoopLab"){
		Sketchup.active_model.select_tool RND_Extensions::RND_LoopLab::LoopLab.new
		}
		rnd_looplab_cmd.small_icon = "img/rnd_looplab_1_16.png"
		rnd_looplab_cmd.large_icon = "img/rnd_looplab_1_24.png"
		rnd_looplab_cmd.tooltip = "LoopLab"
		rnd_looplab_cmd.status_bar_text = "Select edges that are part of a loop."
		rnd_looplab_cmd.menu_text = "LoopLab"
		rnd_looplab_tb = rnd_looplab_tb.add_item rnd_looplab_cmd
		rnd_looplab_tb.show
	end
	#
	#------Add Context Menu-------------------------
	UI.add_context_menu_handler{|menu|
		#
		menu.add_item('LoopLab'){
			Sketchup.active_model.select_tool RND_Extensions::RND_LoopLab::LoopLab.new
		}
	}
	#
	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end
