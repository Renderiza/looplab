module RND_Extensions
	#
	module RND_LoopLab
		#
		class LoopLab
			#
			def activate()
				#
				model = Sketchup.active_model
				sel = model.selection
				edges = sel.grep(Sketchup::Edge)
				#
				use = edges.last
				rerun = edges.last
				#
				run = true
				stp1 = false
				stp2 = false
				#
				skip = true
				#
				model.start_operation('LoopLab')
					#
					begin
						#
						edge = use
						faces = edge.faces
						v1 = edge.start
						v2 = edge.end
						v1_edges = v1.edges
						v2_edges = v2.edges
						#
						break if v1_edges.length > 4 || v2_edges.length > 4
						#
						v1_edges.to_a.each { |ne|
							if not ne.used_by? faces[0] and not ne.used_by? faces[1] and not sel.include?(ne)
									use = ne
									sel.add ne
									stp1 = false
									break 
							else
									stp1 = true
							end
						}
						#
						if skip == true
							skip = false
							next
						end
						#
						v2_edges.to_a.each { |ne|
							if not ne.used_by? faces[0] and not ne.used_by? faces[1] and not sel.include?(ne)
								use = ne  
								sel.add ne
								stp2 = false
								break
							else
								stp2 = true
							end
						}
						#
						if stp1 and stp2
							run = false
						end
						#
					end while run
					#
					#
					#
					run = true
					stp1 = false
					stp2 = false
					skip = true
					#
					begin
						#
						edge = rerun
						faces = edge.faces
						v1 = edge.start
						v2 = edge.end
						v1_edges = v1.edges
						v2_edges = v2.edges
						#
						break if v1_edges.length > 4 || v2_edges.length > 4
						#
						v1_edges.to_a.each { |ne|
							if not ne.used_by? faces[0] and not ne.used_by? faces[1] and not sel.include?(ne)
								rerun = ne
								sel.add ne
								stp1 = false
								break
							else
								stp1 = true
							end
						}
						#
						if skip == true
							skip = false
							next
						end
						#
						v2_edges.to_a.each { |ne|
							if not ne.used_by? faces[0] and not ne.used_by? faces[1] and not sel.include?(ne)
								rerun = ne
								sel.add ne
								stp2 = false
								break
							else
								stp2 = true
							end
						}
						#
						if stp1 and stp2
							run = false
						end
						#
					end while run
					#
				model.commit_operation
				#
				Sketchup.send_action('selectSelectionTool:')
				#
			end
			#
		end #class
		#
	end #module
	#
end #module
#
file_loaded( File.basename(__FILE__) )
